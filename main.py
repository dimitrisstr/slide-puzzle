import os
import pygame as pg
from random import randint

text_color = (255, 255, 255)

# screen variables
min_screen_coord = (0, 0)
max_screen_coord = (768, 768)
image_size = (512, 512)

grid_coord = ((max_screen_coord[0] - image_size[0]) / 2,
              (max_screen_coord[1] - image_size[1]) / 2)

time_coord = [max_screen_coord[0] / 2, 32]
message_coord = [max_screen_coord[0] / 2, max_screen_coord[1] - 64]
FPS = 30

# key repeat
delay = 100  # milliseconds
interval = 50  # milliseconds

# game resources
font_file = 'SadanaSquare.ttf'


def write(font, message, color):
    text = font.render(str(message), True, color)
    text = text.convert_alpha()

    return text


class Grid:
    def __init__(self, image, image_size, grid_size):
        tile_size = image_size[0] / grid_size
        image = pg.transform.scale(image, image_size)

        self.grid = []
        self.empty_tile_index = (grid_size - 1, grid_size - 1)
        self.empty_tile_value = (grid_size * grid_size) - 1
        start_index = 0
        for i in range(grid_size):
            self.grid.append(list(range(start_index, start_index + grid_size)))
            start_index += grid_size

        self.tile_list = []
        for i in range(grid_size):
            for j in range(grid_size):
                tile_surface = image.subsurface\
                    (j * tile_size, i * tile_size, tile_size, tile_size)
                self.tile_list.append(tile_surface)

        self.grid_size = grid_size
        self.tile_size = tile_size

    def shuffle(self):
        functions_list = [self.move_up, self.move_down, self.move_left, self.move_right]
        for i in range(500):
            functions_list[randint(0, 3)]()

    def move_up(self):
        # move the empty tile up
        row, column = self.empty_tile_index
        if row != 0:
            temp = self.grid[row - 1][column]
            self.grid[row - 1][column] = self.grid[row][column]
            self.grid[row][column] = temp
            self.empty_tile_index = (row - 1, column)

    def move_down(self):
        # move the empty tile down
        row, column = self.empty_tile_index
        if row != self.grid_size - 1:
            temp = self.grid[row + 1][column]
            self.grid[row + 1][column] = self.grid[row][column]
            self.grid[row][column] = temp
            self.empty_tile_index = (row + 1, column)

    def move_left(self):
        # move the empty tile left
        row, column = self.empty_tile_index
        if column != 0:
            temp = self.grid[row][column - 1]
            self.grid[row][column - 1] = self.grid[row][column]
            self.grid[row][column] = temp
            self.empty_tile_index = (row, column - 1)

    def move_right(self):
        # move the empty tile right
        row, column = self.empty_tile_index
        if column != self.grid_size - 1:
            temp = self.grid[row][column + 1]
            self.grid[row][column + 1] = self.grid[row][column]
            self.grid[row][column] = temp
            self.empty_tile_index = (row, column + 1)

    def is_puzzle_solved(self):
        index = 0
        for i in range(self.grid_size):
            for j in range(self.grid_size):
                if self.grid[i][j] != index:
                    return False
                index += 1
        return True

    def show(self, screen, coord):
        x, y = coord
        start_x = x
        for i in range(self.grid_size):
            for j in range(self.grid_size):
                tile_index = self.grid[i][j]
                if tile_index != self.empty_tile_value:
                    screen.blit(self.tile_list[tile_index], (x, y))
                x += self.tile_size
            x = start_x
            y += self.tile_size


def main():
    pg.init()
    pg.display.set_caption("Slide Puzzle")
    screen = pg.display.set_mode(max_screen_coord)
    pg.key.set_repeat(delay, interval)

    # load font
    font = pg.font.Font(os.path.join('fonts', font_file), 38)

    background = pg.Surface(screen.get_size())
    background.fill((0, 0, 0))
    image = pg.image.load(os.path.join('sprites', 'image.png')).convert_alpha()

    game_running = True
    while game_running:
        grid = Grid(image, image_size, 4)
        screen.blit(background, min_screen_coord)
        grid.show(screen, grid_coord)
        text_surface = write(font, 'PRESS SPACE TO START', text_color)
        screen.blit(text_surface, (time_coord[0] - text_surface.get_width() / 2, time_coord[1]))
        pg.display.flip()
        while True:
            event = pg.event.wait()
            if event.type == pg.KEYDOWN and event.key == pg.K_SPACE:
                break
            if (event.type == pg.KEYDOWN and event.key == pg.K_ESCAPE) or event.type == pg.QUIT:
                exit()

        total_time = 0.0
        clock = pg.time.Clock()
        game_over = False
        grid.shuffle()
        while not game_over:
            game_over = grid.is_puzzle_solved()
            for event in pg.event.get():
                if event.type == pg.QUIT:
                    exit()
                elif event.type == pg.KEYDOWN and event.key == pg.K_ESCAPE:
                    game_over = True
                elif event.type == pg.KEYDOWN and event.key == pg.K_UP:
                    grid.move_down()
                elif event.type == pg.KEYDOWN and event.key == pg.K_DOWN:
                    grid.move_up()
                elif event.type == pg.KEYDOWN and event.key == pg.K_LEFT:
                    grid.move_right()
                elif event.type == pg.KEYDOWN and event.key == pg.K_RIGHT:
                    grid.move_left()

            total_time += clock.tick(FPS) / 1000.0  # convert to seconds

            screen.blit(background, min_screen_coord)
            grid.show(screen, grid_coord)
            time_string = "TIME " + '{0:02d}'.format(int(total_time // 60)) \
                          + ":" + '{0:02d}'.format(int(total_time % 60))
            time_surface = write(font, time_string, text_color)
            screen.blit(time_surface, (time_coord[0] - time_surface.get_width() / 2, time_coord[1]))
            pg.display.flip()

        if grid.is_puzzle_solved():
            text_surface = write(font, 'PUZZLE SOLVED', (0, 255, 0))
            screen.blit(text_surface, (message_coord[0] - text_surface.get_width() / 2, message_coord[1]))
            pg.display.flip()
            clock.tick(0.5)


if __name__ == '__main__':
    main()
